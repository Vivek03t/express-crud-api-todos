const todosControlller = require('../controllers/todosController');
const {
  createTodoSchema,
  updateTodoSchema,
  deleteTodoSchema,
} = require('../middleware/validateSchemas');
const validate = require('../middleware/validate');

// router
const router = require('express').Router();

// use routers
router.post('/', validate(createTodoSchema), todosControlller.addTodo);

router.get('/', todosControlller.getAllTodos);

router.get('/:id', todosControlller.getTodo);

router.put('/:id', validate(updateTodoSchema), todosControlller.updateTodo);

router.delete('/:id', validate(deleteTodoSchema), todosControlller.deleteTodo);

module.exports = router;
