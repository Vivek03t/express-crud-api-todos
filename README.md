# Express CRUD API for Todos

This project is a simple Express.js-based HTTP API that implements CRUD (Create, Read, Update, Delete) operations for managing todos. Todos are stored in a PostgreSQL database using Sequelize as the ORM (Object-Relational Mapping) library. Validation for incoming data is performed using Yup.

## Table of Contents

- [Express CRUD API for Todos](#express-crud-api-for-todos)
  - [Table of Contents](#table-of-contents)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
    - [Usage](#usage)
    - [API Endpoints](#api-endpoints)
    - [Validation](#validation)
    - [Database](#database)
    - [Error Handling](#error-handling)
    - [Contributing](#contributing)
    - [License](#license)

## Getting Started

### Prerequisites

Before running the project, ensure you have the following prerequisites installed:

- [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/) (Node Package Manager)
- [PostgreSQL](https://www.postgresql.org/) database server

### Installation

1. Clone the repository:

   ```bash```
   git remote add origin git@gitlab.com:Vivek03t/express-crud-api-todos.git
   cd express-crud-api-todos

2. Install the project dependencies:

    ```bash```
    npm install

    Configure the PostgreSQL database connection by editing the config/dbConfig.js file. Update the database name, username, and password as per your PostgreSQL setup.

3. Create the PostgreSQL database for the project.

    ```bash```
    createdb your_database_name
4. Run the database migrations to create the necessary tables:

    ```bash```
    npm run db:migrate

5.  Usage

    Start the Express server:

    ```bash```
    npm start

    By default, the server will run on port 8080. You can access the API at http://localhost:8080.

6. API Endpoints
   
    The API provides the following endpoints:

    - Fetch todo list: GET /todos
        - Returns an array of existing todos.
  
    - Fetch Todo Detail: GET /todos/:id
        - Returns the todo with the specified ID. Returns a 404 error if no todo is found.
  
    - Create Todo: POST /todos
        - Creates a new todo and returns it.
  
    - Example Request Body:

        ```json```
        {
        "text": "Learn SQL",
        "isCompleted": false
        }
        
    - Update Todo: PUT /todos/:id
        - Updates a particular todo and returns the updated todo.
  
    - Delete Todo: DELETE /todos/:id
        - Deletes the specified todo by ID.
    
    - Validation
        - Validation for incoming data is performed using Yup. The validation schema ensures that requests meet the required format and data types. If a request fails validation, a 400 Bad Request response is returned.

    - Database
        - Todos are stored in a PostgreSQL database using Sequelize as the ORM. The models/todosModel.js file defines the schema for the Todo model. Database migrations are managed using Sequelize migrations.

    - Error Handling
        - The API returns appropriate HTTP status codes for success and error responses. For instance, a 404 status code is returned when a requested todo is not found.

    - Contributing
        - Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or create a pull request.

    - License
        - This project is licensed under the MIT License. See the LICENSE file for details.
