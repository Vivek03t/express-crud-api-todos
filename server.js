const express = require('express');
const app = express();

// middlewares
const {
  pathError,
  errorLogger,
  sendError,
} = require('./middleware/errorHandler');

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

// routers
const router = require('./routes/todosRouter.js');
app.use('/api/todos', router);

// testing api
app.get('/', (req, res) => {
  res.json({ message: 'Hello from api' });
});

// errro handler
app.use(errorLogger);
app.use(sendError);
app.use(pathError);

// port
const PORT = process.env.PORT || 8080;

//server
app.listen(PORT, () => {
  console.log(`Server is running on ${PORT}`);
});
