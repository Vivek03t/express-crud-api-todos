module.exports = {
  HOST: 'localhost',
  USER: 'your_userame',
  PASSWORD: 'your_secretKey',
  DB: 'your_db_name',
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
