require('dotenv').config();
const dbConfig = require('../config/dbConfig.js');
const { Sequelize, DataTypes } = require('sequelize');

const db_user = process.env.DB_USER;
const db_password = process.env.DB_PASSWORD;
const db_name = process.env.DB_NAME;

const sequelize = new Sequelize(db_name, db_user, db_password, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

sequelize
  .authenticate()
  .then(() => {
    console.log('connected..');
  })
  .catch((error) => {
    console.error('Error' + error);
  });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.todo = require('./todosModel.js')(sequelize, DataTypes);

db.sequelize.sync({ alter: true }).then(() => {
  console.log('yes re-sync done!');
});

module.exports = db;
