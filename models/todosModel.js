module.exports = (Sequelize, DataTypes) => {
  const Todo = Sequelize.define('todo', {
    text: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isCompleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  });

  return Todo;
};
