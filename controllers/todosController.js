const db = require('../models');

// create main model
const Todo = db.todo;

// 1. create todo
const addTodo = async (req, res) => {
  try {
    const todo = await Todo.create(req.body);
    res.status(201).json(todo);
  } catch (error) {
    res.status(400).json({ message: 'Bad request' });
  }
};

// 2. get all todos
const getAllTodos = async (req, res) => {
  let todos = await Todo.findAll({});
  res.status(200).json(todos);
};

// 3. get single todo detail
const getTodo = async (req, res, next) => {
  let { id } = req.params;
  if (isNaN(id)) {
    return res.status(400).json({ message: `Invalid id : ${id}` });
  }
  const todo = await Todo.findByPk(id);

  if (!todo) {
    return res.status(404).json({ message: 'Not found' });
  }
  res.status(200).json(todo);
};

// 4. update todo
const updateTodo = async (req, res) => {
  let { id } = req.params;
  const todo = await Todo.findByPk(id);

  if (!todo) {
    throw new Error('Not Found');
  }

  try {
    await todo.update(req.body);
    res.status(200).json(todo);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

// 5. delete todo
const deleteTodo = async (req, res) => {
  const { id } = req.params;
  const todo = await Todo.findByPk(id);

  if (!todo) {
    return res.status(404).json({ message: 'Not found' });
  }
  await todo.destroy();
  res
    .status(200)
    .json({ message: `id ${req.params.id}todo deleted successfully` });
};

module.exports = {
  addTodo,
  getAllTodos,
  getTodo,
  updateTodo,
  deleteTodo,
};
