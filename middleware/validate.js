const validate = (schema) => {
  return async (req, res, next) => {
    try {
      await schema.validate({
        body: req.body,
        params: req.params,
      });
      return next();
    } catch (error) {
      res.status(400).json({
        type: error.name,
        message: error.message,
      });
    }
  };
};

module.exports = validate;
