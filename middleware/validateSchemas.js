const yup = require('yup');

const createTodoSchema = yup.object({
  body: yup.object({
    text: yup.string().required(),
    isCompleted: yup.boolean().required(),
  }),
});

const updateTodoSchema = yup.object({
  body: yup.object({
    text: yup.string().required(),
    isCompleted: yup.boolean().required(),
  }),
  params: yup.object({
    id: yup.number().required(),
  }),
});

const deleteTodoSchema = yup.object({
  params: yup.object({
    id: yup.number().required(),
  }),
});

module.exports = {
  createTodoSchema,
  updateTodoSchema,
  deleteTodoSchema,
};
